<?php

    /*

    Called from dbControl.php

    */

    //Function to add to database

// Function to add a new blog text
function addBlogText($postTitle, $postText, $postAuthor) {
    $pdo = PDO();
    $sql = "INSERT INTO blogikirjoitukset (postTitle, postText, postAuthor) 
            VALUES (:postTitle, :postText, :postAuthor)";
    $stmt = $pdo->prepare($sql);
    $stmt ->execute(array(
        ':postTitle' => $_POST['postTitle'],
        ':postText' => $_POST['postText'],
        ':postAuthor' => $_POST['postAuthor']
        ));
    $_SESSION['success'] = 'Record Added';
    return;
}

// Function to fetch comment
function fetchComments($blog){
    $pdo = PDO();
    $stmt = $pdo->query("SELECT * FROM kommentit where commentBlog = $blog");
    $data = $stmt->fetchALL(PDO::FETCH_ASSOC);
    $return = $data;
    return $return;
}

// Function to add new comment on a blog post
function addComment($commentText, $commentAuthor, $commentBlog) {
    $pdo = PDO();
    $sql = "INSERT INTO kommentit (commentText, commentAuthor, commentBlog) 
            VALUES (:commentText, :commentAuthor, :commentBlog)";
    $stmt = $pdo->prepare($sql);
    $stmt ->execute(array(
        ':commentText' => $_POST['commentText'],
        ':commentAuthor' => $_POST['commentAuthor'],
        ':commentBlog' => $_POST['commentBlog']
        ));
    $_SESSION['success'] = 'Record Added';
    return;
}
?>