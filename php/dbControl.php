<?php
    /*

    Called from javascript
    Runs functions from dbFunctions.php

    */

    require_once("pdo.php");
    require_once("dbFunctions.php");
    require_once("logWriter.php");

    $operation = $_POST['operation'];
    $return;

    switch ($operation) {
        case 'addBlogText':
            $postTitle = $_POST['postTitle'];
            $postText = $_POST['postText'];
            $postAuthor = $_POST['postAuthor'];

            logWrite("addBlogText(), postTitle = $postTitle, postText = $postText, postAuthor = $postAuthor ");
            $return = addBlogText($postTitle, $postText, $postAuthor);
            break;
        
        case 'fetchComments':
            $blog = $_POST['blog'];

            logWrite("fetchComments(), blog = $blog");
            $return = fetchComments($blog);
            echo(json_encode($return));
            break;
        
        case 'addComment':
            $commentText = $_POST['commentText'];
            $commentAuthor = $_POST['commentAuthor'];
            $commentBlog = $_POST['commentBlog'];

            logWrite("addComment(), $commentText, $commentAuthor, $commentBlog");
            $return = addComment($commentText, $commentAuthor, $commentBlog);
            break;

        default:
            logWrite("Command Not Found");
            break;
    }
?>