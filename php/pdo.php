<?php

require_once("logWriter.php");

function PDO() {
  $servername = "localhost";
  $username = "root";
  $password = "";
  try {
    $pdo = new PDO("mysql:host=$servername;dbname=blogisivu", $username, $password);
    // set the PDO error mode to exception
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch(PDOException $e) {
    addToLog("Connection failed: " . $e->getMessage());
  }
  return $pdo;
}
?>