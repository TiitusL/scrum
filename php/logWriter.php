<?php

function logWrite($text) {
    $dateTimeStr = date("Y-m-d H:i:s");
    $fileName = date("Ymd")."_log.txt";
    $file = fopen("./logs/$fileName", "a");
    $logStr = $dateTimeStr." ".$text."\r\n";
    fwrite($file, $logStr);
    fclose($file);
}

?>
