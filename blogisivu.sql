-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 23, 2022 at 09:34 AM
-- Server version: 10.4.25-MariaDB
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blogisivu`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogikirjoitukset`
--

CREATE TABLE `blogikirjoitukset` (
  `postID` int(11) NOT NULL COMMENT 'Kirjoituksen ID',
  `postTitle` varchar(50) NOT NULL COMMENT 'Kirjoituksen otsikko',
  `postText` varchar(5000) NOT NULL COMMENT 'Kirjoituksen Teksti',
  `postAuthor` int(50) NOT NULL COMMENT 'Kirjoittaja (ID)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blogikirjoitukset`
--

INSERT INTO `blogikirjoitukset` (`postID`, `postTitle`, `postText`, `postAuthor`) VALUES
(1, '123', '123', 0);

-- --------------------------------------------------------

--
-- Table structure for table `kayttajatiedot`
--

CREATE TABLE `kayttajatiedot` (
  `usID` int(11) NOT NULL COMMENT 'Käyttäjä ID / AutoIncrement',
  `usNimi` varchar(50) NOT NULL COMMENT 'Käyttäjänimi',
  `usSala` varchar(50) NOT NULL COMMENT 'Salasana',
  `usEmail` varchar(50) NOT NULL COMMENT 'Email'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Tallentaa käyttäjätiedot';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogikirjoitukset`
--
ALTER TABLE `blogikirjoitukset`
  ADD PRIMARY KEY (`postID`);

--
-- Indexes for table `kayttajatiedot`
--
ALTER TABLE `kayttajatiedot`
  ADD PRIMARY KEY (`usID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogikirjoitukset`
--
ALTER TABLE `blogikirjoitukset`
  MODIFY `postID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Kirjoituksen ID', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kayttajatiedot`
--
ALTER TABLE `kayttajatiedot`
  MODIFY `usID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Käyttäjä ID / AutoIncrement';
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
