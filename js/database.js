// Calls dbControl.php with $operation as "addToDB"

// Calls dbControl.php with $operation as "addBlogText"
function addBlogText() {
    const controller ="../php/dbControl.php";
    console.log("Adding to db...");
    $.post(controller, {
        operation: "addBlogText",
        postTitle: $("#postTitle").val(),
        postText: $("#postText").val(),

        // Blog writer's userID (to be replaced)
        postAuthor: "0100100"
    }).done( data => {console.log(data)});
}

// Calls dbControl.php with $operation as "fetchComments"
// Runs loadComments()
const fetchComments = () => {
    const controller = "../../php/dbControl.php";
    $.post(controller, {
        operation: "fetchComments",
        // Blog post ID on which to fetch comments (to be replaced)
        blog: "1" 
    }).done(data => {loadComments(data)})};


// Loads comments on the page
function loadComments(data) {
    let jsonData = JSON.parse(data);
    
    /*
        <div id="commentsField">
            <div class="actualComments">
                <p>Comment</p>
    */

    // For loop to create div for each comment with class as actualComments
    for(let i = 0; i < jsonData.length; i++){
        let commentBox = document.getElementById("commentsField");
        let div = document.createElement("div")
        let commentText = document.createElement("p");
        let actualText = document.createTextNode(jsonData[i].commentText);

        commentText.appendChild(actualText);
        div.append(commentText);
        commentBox.appendChild(div);

        div.setAttribute("class", "actualComments")
        
    }
}

// Calls dbControl with $operation as "addComments"
function addComment(){
    const controller ="../../php/dbControl.php";
    console.log("Adding comment...");
    $.post(controller, {
        operation: "addComment",
        commentText: $("#commentText").val(),

        // Commentor's user ID (to be replaced)
        commentAuthor: "5",             
        // ID of the blog post where the comment is to be posted (to be replaced)
        commentBlog: "1"                
    }).done( data => {console.log(data)});
}